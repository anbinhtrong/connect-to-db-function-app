using System;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Dapper;
using System.Data.SqlClient;
using System.Configuration;

using System.Threading.Tasks;
using Microsoft.Azure;
using Microsoft.Extensions.Configuration;
using Microsoft.WindowsAzure.Storage;

namespace GetDataFromDatabase
{
    public static class GetDataFromTable
    {
        //private static SqlConnection connection = new SqlConnection();
        [FunctionName("GetDataFromTable")]
        public static async Task Run([TimerTrigger("0 */2 * * * *")]TimerInfo myTimer, TraceWriter log, ExecutionContext context)
        {
            log.Info($"C# Timer trigger function executed at: {DateTime.Now}");
            var successful = true;
            try
            {
                var cnnString = GetConnectionString(context);

                using (var connection = new SqlConnection(cnnString))
                {
                    connection.Open();
                    var text = "UPDATE SalesLT.SalesOrderHeader " +
                               "SET [Status] = 5  WHERE ShipDate < GetDate();";

                    using (var cmd = new SqlCommand(text, connection))
                    {
                        // Execute the command and log the # rows affected.
                        var rows = await cmd.ExecuteNonQueryAsync();
                        log.Info($"{rows} rows were updated");
                    }

                }
            }
            catch
            {
                successful = false;
            }
        }

        /// <summary>
        /// get connection string
        /// </summary>
        /// <returns></returns>
        private static string GetConnectionString(ExecutionContext context)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(context.FunctionAppDirectory)
                .AddJsonFile("local.settings.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build();
            var cstr = config.GetConnectionString("SqlConnectionString");
            var setting1 = config["Setting1"];
            return cstr;
        }
    }
}
